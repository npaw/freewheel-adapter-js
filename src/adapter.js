/* global tv */
var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Freewheel = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this._getAdInstanceMethodCall('getPlayheadTime')
  },

  /** Override to return video duration */
  getDuration: function () {
    return this._getAdInstanceMethodCall('getDuration')
  },

  /** Override to return title */
  getTitle: function () {
    try {
      if (this.titles) {
        return this.titles[this._getAdNumber()]._adId
      }
    } catch (err) {
      // youbora.Log.debug('Cant get title')
    }
    return 'unknown'
  },

  /** Override to return resource URL. */
  getResource: function () {
    var ret = null
    var creativeRendition = this._getAdInstanceMethodCall('getActiveCreativeRendition')
    if (creativeRendition) {
      if (typeof creativeRendition.getPrimaryCreativeRenditionAsset === 'function') {
        const asset = creativeRendition.getPrimaryCreativeRenditionAsset();
        if (asset && typeof asset.getUrl === 'function') {
          ret = asset.getUrl()
        }
      } else if (typeof creativeRendition.getWrapperUrl === 'function') {
        ret = creativeRendition.getWrapperUrl()
      }
    }
    return ret
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    var ret = null
    if (typeof tv !== 'undefined' && tv.freewheel && tv.freewheel.SDK) {
      ret = tv.freewheel.SDK.version
    }
    return ret
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Freewheel'
  },

  getPosition: function () {
    return this.position
  },

  getIsVisible: function () {
    if (!this.contentPlayer) {
      for (var key in this.player) {
        var element = this.player[key]
        if (element && element.videoHeight && element.clientHeight) {
          this.contentPlayer = element
          break
        }
      }
    }
    return youbora.Util.calculateAdViewability(this.contentPlayer)
  },

  getAudioEnabled: function () {
    for (var key in this.player) {
      var element = this.player[key]
      if (element && element.adManager && element.adManager._context && element.adManager._context.getAdVolume) {
        return !!element.adManager._context.getAdVolume()
      }
    }
  },

  getGivenAds: function () {
    return this.slot.getAdCount()
  },

  getBreaksTime: function () {
    var slots = this.getSlots()
    if (!slots) return null
    var times = []
    for (var slotindex in slots) {
      var slot = slots[slotindex]
      if (slot.getAdCount() > 0) {
        var ads = slot.getAdInstances()
        for (var adindex in ads) {
          if (ads[adindex]._creativeRenditions[0]._baseUnit === 'video') {
            var duration = this.plugin.getDuration()
            var position = slot.getTimePosition()
            times.push(duration && position > duration ? duration : position)
            break
          }
        }
      }
    }
    return times
  },

  getGivenBreaks: function () {
    var slots = this.getSlots()
    if (!slots) return null
    var breaks = 0
    for (var slotindex in slots) {
      var slot = slots[slotindex]
      if (slot.getAdCount() > 0) {
        var ads = slot.getAdInstances()
        for (var adindex in ads) {
          if (ads[adindex]._creativeRenditions[0]._baseUnit === 'video') {
            breaks++
            break
          }
        }
      }
    }
    return breaks
  },

  getCreativeId: function () {
    var ret = null
    if (this.slot) {
      var instances = this.slot.getAdInstances()
      var adnumber = this._getAdNumber()
      if (instances && instances[adnumber]) {
        ret = instances[adnumber]._creativeId
      }
    }
    return ret
  },

  getSlots: function () {
    if (this.player.currentAdContext) {
      return this.player.currentAdContext._adResponse.getTemporalSlots()
    } else {
      for (var key in this.player) {
        var element = this.player[key]
        if (element && element._adResponse && element._adResponse.getTemporalSlots) {
          return element
        }
      }
    }
    return null
  },

  _getAdNumber: function () {
    return (this.plugin.requestBuilder.lastSent.adNumber || 1) - 1
  },

  _getAdInstanceMethodCall: function (methdodName) {
    var ret = null
    if (this.slot) {
      var instances = this.slot.getAdInstances()
      var adNumber = this._getAdNumber()
      if (instances && instances[adNumber] && typeof instances[adNumber][methdodName] === 'function') {
        ret = instances[adNumber][methdodName]()
      }
    }
    return ret
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    this.canRemoveListeners = true

    // References
    this.events = tv.freewheel.SDK
    this.manifestNoResponse = [
      this.events.ERROR_SECURITY,
      this.events.ERROR_TIMEOUT
    ]
    this.manifestEmpty = [
      this.events.ERROR_NO_AD_AVAILABLE,
      this.events.ERROR_VAST_NO_AD
    ]
    this.manifestWrong = [
      this.events.ERROR_VAST_VERSION_NOT_SUPPORTED,
      this.events.ERROR_VAST_WRAPPER_LIMIT_REACH,
      this.events.ERROR_VAST_XML_PARSING,
      this.events.ERROR_PARSE
    ]
    this.ignoredErrors = [
      this.events.ERROR_ADINSTANCE_UNAVAILABLE
    ]
    this.references = {}
    this.references[this.events.EVENT_AD] = this.logListener.bind(this)
    this.references[this.events.EVENT_ERROR] = this.errorListener.bind(this)
    this.references[this.events.EVENT_SLOT_STARTED] = this.slotListener.bind(this)
    this.references[this.events.EVENT_SLOT_ENDED] = this.closeListener.bind(this)
    for (var key in this.manifestNoResponse) {
      this.references[key] = this.noResponseManifestListener.bind(this)
    }
    for (var key2 in this.manifestEmpty) {
      this.references[key2] = this.manifestEmptyListener.bind(this)
    }
    for (var key3 in this.manifestWrong) {
      this.references[key3] = this.manifestWrongListener.bind(this)
    }

    for (var key4 in this.references) {
      this.player.currentAdContext.addEventListener(key4, this.references[key4])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.currentAdContext.removeEventListener(key, this.references[key])
      }
      this.references = {}
    }
  },

  noResponseManifestListener: function (e) {
    this.fireManifest(youbora.Constants.ManifestError.NO_RESPONSE, 'No response')
  },

  manifestEmptyListener: function (e) {
    this.fireManifest(youbora.Constants.ManifestError.EMPTY, 'Empty manifest')
  },

  manifestWrongListener: function (e) {
    this.fireManifest(youbora.Constants.ManifestError.WRONG, 'Wrong manifest format')
  },

  logListener: function (e) {
    youbora.Log.debug(e.subType)
    if (e.errorCode || e.errorInfo || e.errorModule) {
      this.errorListener(e)
      return
    }
    switch (e.subType) {
      case this.events.EVENT_AD_PAUSE:
        this.pauseListener(e)
        break
      case this.events.EVENT_AD_RESUME:
        this.resumeListener(e)
        break
      case this.events.EVENT_AD_IMPRESSION_END:
        this.endedListener(e)
        break
      case this.events.EVENT_AD_INITIATED:
        this.playListener(e)
        break
      case this.events.EVENT_AD_IMPRESSION:
        this.playingListener(e)
        break
      case this.events.EVENT_AD_SKIPPED:
        this.skipListener(e)
        break
      case this.events.EVENT_SLOT_STARTED:
        this.slotListener(e)
        break
      case this.events.EVENT_AD_CLOSE:
      case this.events.EVENT_SLOT_ENDED:
        this.closeListener(e)
        break
      case this.events.EVENT_AD_CLICK:
        this.clickListener(e)
        break
      case this.events.EVENT_AD_FIRST_QUARTILE:
        this.fireQuartile(1)
        break
      case this.events.EVENT_AD_MIDPOINT:
        this.fireQuartile(2)
        break
      case this.events.EVENT_AD_THIRD_QUARTILE:
        this.fireQuartile(3)
        break
      case this.events.ERROR_SECURITY:
      case this.events.ERROR_TIMEOUT:
        this.noResponseManifestListener(e)
        break
      case this.events.ERROR_NO_AD_AVAILABLE:
      case this.events.ERROR_VAST_NO_AD:
        this.manifestEmptyListener(e)
        break
      case this.events.ERROR_VAST_VERSION_NOT_SUPPORTED:
      case this.events.ERROR_VAST_WRAPPER_LIMIT_REACH:
      case this.events.ERROR_VAST_XML_PARSING:
      case this.events.ERROR_PARSE:
        this.manifestWrongListener(e)
        break
    }
  },

  slotListener: function (e) {
    this.slot = e.slot
    var adapter = this.plugin.getAdapter()
    this.disable = false
    switch(this.slot.getTimePositionClass()) {
      case 'POSTROLL':
        this.position = youbora.Constants.AdPosition.Postroll
        break
      case 'MIDROLL':
        this.position = youbora.Constants.AdPosition.Midroll
        break
      case 'PREROLL':
        if (adapter && adapter.flags.isJoined) {
          this.position = youbora.Constants.AdPosition.Midroll
        } else {
          this.position = youbora.Constants.AdPosition.Preroll
        }
        break
      default:
        this.disable = true
    }
    if (!this.disable) {
      this.titles = this.slot.getAdInstances()
      this.unregister()
      this.plugin.fireInit()
      this.fireInit()
    }
  },

  playListener: function (e) {
    if (!this.disable) {
      this.fireStart()
    }
  },

  playingListener: function (e) {
    if (!this.disable) {
      this.fireStart()
      this.fireJoin()
      this.fireResume()
    }
  },

  pauseListener: function (e) {
    this.firePause()
  },

  resumeListener: function (e) {
    this.playListener()
    this.fireResume()
  },

  endedListener: function (e) {
    this.fireStop()
  },

  skipListener: function (e) {
    this.fireSkip()
  },

  clickListener: function (e) {
    var url = e.adInstance.getEventCallbackUrls(tv.freewheel.SDK.EVENT_AD_CLICK, tv.freewheel.SDK.EVENT_TYPE_CLICK)[0]
    var now = new Date().getTime()
    if (this.lastUrl === url && now < ((this.lastTime || 0) + 2000)) {
      return
    }
    this.lastUrl = url
    this.lastTime = now
    this.fireClick(url)
  },

  closeListener: function (e) {
    this.register()
    if (this.position === youbora.Constants.AdPosition.Postroll && !this.disable) {
      this.plugin.fireStop()
    }
  },

  errorListener: function (e) {
    this.fireError(e.errorCode || e.subType, e.errorInfo)
    this.fireStop()
    this.register()
  },

  register: function (e) {
    var adapter = this.plugin.getAdapter()
    if (adapter && !this.canRemoveListeners) {
      adapter.registerListeners()
      adapter.fireResume()
      this.canRemoveListeners = true
    }
  },

  unregister: function(e) {
    var adapter = this.plugin.getAdapter()
    if (adapter && this.canRemoveListeners) {
      this.canRemoveListeners = false
      adapter.unregisterListeners()
      adapter.firePause()
    }
  },

  changeVideo: function (e) {
    if (this.flags.isStarted || this.flags.isInited) {
      this.fireStop()
      this.plugin.fireStop()
      this.register()
    }
  }
})

module.exports = youbora.adapters.Freewheel
