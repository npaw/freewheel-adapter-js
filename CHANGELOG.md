## [6.8.5] - 2023-11-29
### Fixed
- Fix bug caused by 

## [6.8.4] - 2023-10-09
### Fixed
- Fix slot title bug.

## [6.8.3] - 2023-10-09
### Fixed
- Ad's slotListener issue.

## [6.8.2] - 2022-10-03
### Library
- Packaged with `lib 6.8.28`

## [6.8.1] - 2022-03-30
### Added
- Split unregister player listeners code
### Library
- Packaged with `lib 6.8.15`

## [6.8.0] - 2021-12-03
### Library
- Packaged with `lib 6.8.9`

## [6.7.1] - 2020-12-09
### Added
- Manifest error listeners
- Code refactor

## [6.7.0] - 2020-10-28
### Library
- Packaged with `lib 6.7.21`

## [6.5.2] - 2020-01-28
### Fixed
 - AdClick event being sent twice
### Library
- Packaged with `lib 6.5.25`

## [6.5.1] - 2019-07-01
### Fixed
 - Errors accessing to player parameters with unexpected names.
### Library
- Packaged with `lib 6.5.5`

## [6.5.0] - 2019-05-31
### Library
- Packaged with `lib 6.5.2`

## [6.4.4] - 2019-04-29
### Fixed
- When ad resource is not available, tries to get the wrapper url

## [6.4.3] - 2019-04-29
### Fixed
- Now obtains the actual ad resource, not the content one.

## [6.4.3] - 2019-04-29
### Fixed
- Now obtains the actual ad resource, not the content one.

## [6.4.2] - 2019-03-28
### Added
- Flag to wait REQUEST_COMPLETE event before reporting ad events to prevent "fake" ads.

## [6.4.1] - 2018-12-10
### Added
- Adclick url

## [6.4.0] - 2018-10-04
### Added
- Adapter refactor
- `changeVideo` method to call manually when using playlists.

## [6.3.2] - 2018-10-04
### Added
- Fix for duration and playhead

## [6.3.1] - 2018-08-24
### Added
- Fix for adpause and adresume

## [6.3.0] - 2018-08-13
### Added
- Error listening in EVENT_AD event

## [6.2.3] - 2018-06-12
### Added
- Delayed adinit and adjoin (removed from slot started)

## [6.2.2] - 2018-06-11
### Added
- Error listeners
- Default case for getters

## [6.2.1] - 2018-06-05
### Fixed
- Included extra stop event and init check

## [6.2.0] - 2018-05-03
### Fixed
- Content jointime calculation with init

## [6.1.4] - 2018-03-26
### Fixed
- Join detection
- Playhead duration and source get from the player

## [6.1.3] - 2018-03-12
### Added
- Added lib again, to ensure compatibility with npm and download minified adapter methods

## [6.1.2] - 2018-03-12
### Removed
- Removed lib dependency, built without it

## [6.1.0] - 2018-03-07
### Library
- Packaged with `lib 6.1.13`
